const express = require('express');
const cors = require('cors'); 

const app = express();

app.use(cors());
app.use(express.static('./dist/roland-gamos-app'));

app.get('/*', function(req, res) {
    res.sendFile('index.html', {root: 'dist/roland-gamos-app/'});
});

const port = process.env.PORT || 4200;

app.listen(port, (err, res) => {
  if (!err) {
    console.log(`service is running on ${port}`);
  }
});