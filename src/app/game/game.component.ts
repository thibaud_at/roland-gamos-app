import { Component, Inject, Input, OnInit } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Artist } from '../interfaces/artist';
import { FirebaseService } from '../services/firebase.service';
import { SpotifyService } from '../services/spotify.service';

@Component({
  selector: 'app-game',
  templateUrl: './game.component.html',
})
export class GameComponent implements OnInit {
  loading: boolean = false;

  @Input() database: any;
  connected_to_db: boolean = false;
  token: string = "";
  connected_to_spotify: boolean = false;

  public display_feats: boolean = false;
  public in_progress: boolean = false;
  difficulties: any[] = [
    { value: 0, viewValue: 'Débutant' },
    { value: 1, viewValue: 'Amateur' },
    { value: 2, viewValue: 'Connaisseur' },
    { value: 3, viewValue: 'Expert' },
  ];
  difficulty: number = this.difficulties[0].value;

  game_artists: Artist[] = [];
  artist_to_check: string = '';
  same_artist: boolean = false;
  timer: number = 30;
  timerInterval: any;
  score: number = 0;
  win: boolean = false;
  gameover: boolean = false;

  artist1: any;
  artist2: any;
  possible_songs: string[] = []
  artist_to_search: string = "";

  artists: Artist[] = [];

  constructor(
    public dialog: MatDialog,
    private _snackBar: MatSnackBar,

    private spotifyService: SpotifyService,
    private firebaseService: FirebaseService,
  ) { }

  ngOnInit() {
    this.loading = true;

    this.spotifyService.connect().then((connected:boolean) => {
      if(connected){
        this.connected_to_spotify = true;
      }

      this.firebaseService.getAllArtists().then((artists: Artist[]) => {
        this.connected_to_db = true;
        this.artists = artists;
        this.loading = false;
      });
    })
  }

  openSettings() {
    const dialogRef = this.dialog.open(SettingsDialog, {
      width: '250px',
      data: {
        display_feats: this.display_feats,
        connected_to_spotify: this.connected_to_spotify,
        connected_to_db: this.connected_to_db
      }
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result != null) this.display_feats = result;
    });
  }

  startGame() : void{
    this.in_progress = true;

    this.score = 0;
    this.win = false;
    this.gameover = false;

    this.artist_to_check = "";
    this.same_artist = false;

    const firstArtist = this.artists
      .sort((a,b) => b.feats.length - a.feats.length)
      .slice(0, (this.artists.length/4)>20 ? 20 : (this.artists.length/4))
      [(Math.floor(Math.random() * Math.floor(5)))];
    this.game_artists.push(firstArtist);

    this.timer=30;
    this.timerInterval = setInterval(() => {if(this.timer>0)this.timer-=1}, 1000);
  }

  checkArtist() : void{
    let artist1 = this.game_artists[this.game_artists.length-1];
    let artist2 = {
      "id": "",
      "name" : this.artist_to_check.toUpperCase(),
      "feats": []
    } as Artist;

    if(this.game_artists.map((a:Artist) => a.name).includes(artist2.name)){
      this.same_artist = true;
    }
    else{
      this.same_artist=false;
      let onDatabase=false;
      let checkOnSpotify=true;
  
      this.artists.forEach((a:Artist) => {
        if(a.name==artist2.name){
          onDatabase=true;
          artist2=a;
          if(artist2.feats.includes(artist1.id)){
            checkOnSpotify=false;
            this.nextArtist(artist2);
          }
        } 
      });
      if(checkOnSpotify){
        this.findSongs(artist1.name, artist2.name).then((songs:string[]) => {
          if(songs.length>0){
            if(artist2.id==""){
              this.firebaseService.insert(artist2.name, [artist1.id]).then((id: string) => {
                artist2.id = id;
                this.artists.push(artist2);
                this.addFeat(artist1, artist2);
                this.nextArtist(artist2);
              })
            }
            else{
              this.addFeat(artist1, artist2);
              this.nextArtist(artist2);
            }
          }
          else{
            this.gameover = true;
          }
        });
      }
    }
  }

  nextArtist(artist: Artist) : void {
    this.game_artists.push(artist);
    let list = document.getElementById("game_list"); if(list)list.scrollTop = list.scrollHeight;
    this.artist_to_check = "";

    this.score++;
    this.findNewArtist(artist);

    this.timer=30;
    clearInterval(this.timerInterval);
    this.timerInterval = setInterval(() => {if(this.timer>0)this.timer-=1}, 1000);
  }

  findNewArtist(artist: Artist){
    const feats = artist.feats;
    let possibles_artists = this.artists.filter((a:Artist) => feats.includes(a.id) && !this.game_artists.includes(a));
    const artist2 = possibles_artists
      .sort((a,b) => a.feats.length - b.feats.length)
      [(Math.floor(Math.random() * Math.floor((4-this.difficulty)*(possibles_artists.length/4))))]
    
    if(artist2){
      this.game_artists.push(artist2);
    }
    else{
      const promises: Promise<any>[] = 
        this.artists
        .filter((a:Artist) => !this.game_artists.includes(a))
        .map((a:Artist) => {
          return new Promise<any>((resolve, reject) => {
            this.findSongs(a.name, artist.name).then((songs:string[]) => {
              resolve({"artist": a, "songs": songs});
            })
          });
        });
  
      Promise.all(promises).then(values => {
        possibles_artists = values
          .filter((as: {artist: Artist, songs: string[]}) => as.songs.length>0)
          .map((as: {artist: Artist, songs: string[]}) => as.artist);

        if(possibles_artists.length>0){
          possibles_artists.forEach((a:Artist) => {
            this.addFeat(artist, a);
          })
          this.game_artists.push(possibles_artists[(Math.floor(Math.random() * Math.floor(possibles_artists.length)))]);
          let list = document.getElementById("game_list"); if(list)list.scrollTop = list.scrollHeight;
        }
        else{
          this.win = true;
          clearInterval(this.timerInterval);
        }
      });
    }
  }

  restartGame() : void{
    this.game_artists = [];
    clearInterval(this.timerInterval);
    this.display_feats=false; 
    this.in_progress = false;
  }

  selectArtist(i: number, artist: any) : void{
    if (i == 1) {
      if (this.artist1 && this.artist1.id == artist.id) {
        this.artist1 = null;
        this.artist2 = null;
        this.possible_songs = [];
      }
      else { this.artist1 = artist; }
    }
    if (i == 2) {
      if (this.artist2 && this.artist2.id == artist.id) {
        this.artist2 = null;
        this.possible_songs = [];
      }
      else {
        this.artist2 = artist;
        this.findSongs(this.artist1.name, this.artist2.name).then((songs: string[]) => {
          this.possible_songs = songs;
          if(songs.length>0){
            this.addFeat(this.artist1, this.artist2);
          }
        });
      }
    }
  }

  hideArtist(artist: Artist) : Artist[]{
    return this.artists.filter(a => a.name != artist.name)
  }

  findSongs(artist1: string, artist2: string) : Promise<string[]>{
    return this.spotifyService.findSongs(artist1,artist2);
  }

  addFeat(artist1: Artist, artist2: Artist) : void{
    this.artists.map((a: Artist) => {
      if(a.id==artist1.id){
        a.feats.push(artist2.id);
        a.feats = a.feats.filter((value:string, index:number, self:any) => self.indexOf(value) === index)
        this.firebaseService.updateFeats(a);
      }
      if(a.id==artist2.id){
        a.feats.push(artist1.id);
        a.feats = a.feats.filter((value:string, index:number, self:any) => self.indexOf(value) === index)
        this.firebaseService.updateFeats(a);
      }
    })
  }
}

@Component({
  selector: 'settings-dialog',
  templateUrl: 'settings-dialog.html',
})
export class SettingsDialog {
  constructor(
    public dialogRef: MatDialogRef<SettingsDialog>,
    @Inject(MAT_DIALOG_DATA) public data: any
  ) { }

  onCloseClick(): void {
    this.dialogRef.close();
  }
}




    // this._snackBar.open((s + " - " + r1 + " feat. " + r2), "Ajouté!", {
    //   duration: 5000,
    // });