import { HttpClient } from "@angular/common/http";
import { Injectable } from "@angular/core";

import { environment } from "src/environments/environment";

import firebase from "firebase/app";
import "firebase/database";
import { Artist } from "../interfaces/artist";

@Injectable()
export class FirebaseService {
  database: any;

  constructor(private http: HttpClient) {
    firebase.initializeApp(environment.FIREBASE_CONFIG);
    this.database = firebase.database();
  }

  getAllArtists(): Promise<Artist[]> {
    let artists: Artist[] = [];
    return new Promise<any>((resolve, reject) => {
      this.database.ref('artists/').once("value", (data: any) => {
        if(data.numChildren() != 0){
          data.forEach((t : any) => {
            artists.push({
              "id": t.key,
              "name": t.val().name,
              "feats": t.val().feats
            })
          });
        }
        resolve(
          artists.sort((a, b) => ('' + a.name).localeCompare(b.name))
        );
      });
    });
  }

  insert(name: string, feats: string[]): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      this.database.ref('artists/').push({
        name: name,
        feats: feats
      }).then((artist:any) => {
        resolve(artist.key);
      })
    });
  }

  updateFeats(artist: Artist): void{
    this.database.ref('artists/'+artist.id).update({
      feats: artist.feats
    });
  }
}
