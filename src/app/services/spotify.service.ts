import { HttpClient, HttpHeaders } from "@angular/common/http";
import { Injectable } from "@angular/core";
import { Observable } from "rxjs";

import { environment } from "src/environments/environment";

declare var require: any;
const querystring = require('query-string');
const axios = require('axios');

@Injectable()
export class SpotifyService {
  token: string = "";

  constructor() { }

  connect(): Promise<boolean> {
    return new Promise<any>((resolve, reject) => {
      axios.post(
        'https://accounts.spotify.com/api/token',
        querystring.stringify({ grant_type: 'client_credentials' }),
        { headers:
          { 
            'Content-Type': 'application/x-www-form-urlencoded',
            'Authorization': 'Basic ' + (btoa(environment.SPOTIFY_CREDENTIALS.client_id + ':' + environment.SPOTIFY_CREDENTIALS.client_secret)) 
          },
        }
      ).then(
        (res:any) => {
          this.token = res.data.access_token;
          resolve(true);
        }
      ).catch(
        (error:any) => {
          resolve(false);
      });
    });
  }

  search(search: string): Promise<any> {
    return new Promise<any>((resolve, reject) => {
      axios.get(
        `https://api.spotify.com/v1/search?q=${search}&type=track`,
        { headers : 
          {
            'Content-Type': 'application/json',
            'Authorization': 'Bearer '+this.token
          }
        }
      ).then(
        (res:any) => {
          resolve(res);
        }
      ).catch(
        (error:any) => {
          reject(error);
      });
    });
  }

  findSongs(artist1: string, artist2: string): Promise<string[]>{
    return new Promise<any>((resolve, reject) => {
      this.search(artist1+" "+artist2).then((res:any) => {
        resolve(
          res.data.tracks.items.length==0
          ? []
          : res.data.tracks.items
              .filter((t: any) => (t.artists.map((a:any) => a.name.toUpperCase()).includes(artist1) && (t.artists.map((a:any) => a.name.toUpperCase()).includes(artist2))))
              .map((t:any) => t.name)
              .filter((value:string, index:number, self:any) => self.indexOf(value) === index)
              .sort((a:string, b:string) => ('' + a).localeCompare(b))
        );
      })
    });
  }
}
