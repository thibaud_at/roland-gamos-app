// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  SPOTIFY_CREDENTIALS: {
    client_id: "57ad3cc0a5344ddd90b70a95eaf9b442",
    client_secret: "ccf8877f234d48938dde4bba4790d597"
  },
  FIREBASE_CONFIG: {
    apiKey: "AIzaSyAVulwVY6_R1sf24fxMWV1q7zH0Xv8dK_Q",  
    authDomain: "roland-gamos-app.firebaseapp.com",
    databaseURL: "https://roland-gamos-app-default-rtdb.europe-west1.firebasedatabase.app/",
    storageBucket: "roland-gamos-app.appspot.com",
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
