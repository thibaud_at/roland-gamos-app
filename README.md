# Roland gamos app

Online version of the famous game "Roland gamos", which first appeared during a [rap game](https://youtu.be/FYklBCCaBmg?t=921) on the youtube channel Red Binks.
Application available [here](https://roland-gamos-app.web.app/)

## Authors

*  **Thibaud Avril-Terrones** - *Initial work*

## Getting started

Roland gamos app is an app built on Angular using a Firebase database.

Built on :
-  [Angular](https://angular.io)
-  [Firebase](https://firebase.google.com/)

Interface built with [Angular Material UI](https://material.angular.io/)

### Launch

```bash
ng serve
```